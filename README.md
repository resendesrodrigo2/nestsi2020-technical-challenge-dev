NEST Summer Internship Challenge 2021
=====================================

Requirements
------------


### Install Git
```
# For linux users
$ sudo apt-get install git
```
Checkout this link to install Git on other platforms: [Install Git](https://git-scm.com/book/en/v1/Getting-Started-Installing-Git)


Clone the repository
--------------------

1. Fork this repository with the name **nestsi21-[your name]**

2. Create a repository in your GitLab account and clone this project

3. When you are done implementing your proposal, email the Internship team with the link to your repository


Challenge
---------
Nest Collective is looking to have a directory of _tascas_ (typical Portuguese small restaurant/cafe/bar) which are located near Downtown and Uptown Offices.

Because you're lazy, instead of doing it manually you're going to create a web platform where you're able to add/edit/remove _tascas_ along with some essential data.


### 1. Build a static web page

Here's a simple wireframe of a Web Application where you can just add new entries

![Directory](screenshot.png)


> Start by building a static web page, using only HTML and CSS, mirroring the mockup above. Don't get too much in detail, just keep the same structure.

Feel free to use any CSS frameworks like Bootstrap, Foundation or similar if you are familiar with any. The same applies for any web framework like ReactJS, Angular or VueJS.


### 2. Build a dynamic web application

Now that the main components are in place, continue by making your application dynamic. Try to dynamically serve the _tascas_ displayed on the page from a database.

You are free to choose any kind of technologies you're familiar with to achieve this task, be it **Ruby on Rails**, **Sinatra**, **ExpressJS**, **Django**, or any other!
Also, use whatever database you're used to, be it relational such as **PostgreSQL** and **MySQL**, document based such as **MongoDB** or any other.

Bring in a new feature by adding an _tasca_ to the directory using the form at the end of the page.
> If you're unfamiliar with server-side web frameworks, try to use Javascript to implement the same functionality.

Extra:

Done already? You must be some kind of genius! Here are a couple of small challenges just to keep you occupied a little longer:

- Create validations for the form fields
- Make a search bar at the top of the listing and implementing filtering by name or address.
- Allow the user to Delete an entry by enabling the "Delete" button
- Upload a photo upon creation
- Click on the list to access a _tasca_ details page
- Create the "Edit" screen and implement its functionality
- Add a new feature that you think would be useful
- Show a Map with the location of each _Tasca_ in the details page


Upload your work
----------------

When you're done, be sure to upload your work back to Github by committing directly to master or to any other branch of your choice.

Don't forget to include instructions on how to set up your web application so we can run it locally.


Thank You
----------------

Speak soon ;)

